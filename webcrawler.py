#!/usr/bin/env python3

"""
Basic webcrawler
To run: type 'python webcrawler.py {url}
Where url is a properly formatted url including protocol (http or https)
"""


from html.parser import HTMLParser
import urllib.request, sys
from urllib.parse import urlparse

class LinkParser(HTMLParser):
    """
    This class represents a page to be parsed
    It handles the finding of link start tags and adds them to the self.links list
    And will find the page title and set self.title to this value
    """
    def handle_starttag(self, tag, attrs):
        """
        For each tag in the html response, this function will check if the tag is an <a> link tag
        and will add its path to the self.links list if it is

        Parameters:
         tag: the tag to be checked
         attrs: the attributes of the tag
        """
        if tag == "a":
           for name, value in attrs:
               if name == "href":
                   #the value of a href attribute will be the link path
                   parsed_uri = urlparse( value )
                   #get the domain of the link
                   link_domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

                   #append the link only if it shares a domain and isnt a '#' tag
                   #links beginning with / are acceptable too as they append onto the current domain
                   if self.domain == link_domain  or ( len(value) > 1 and value[0] == '/' and value[1] != '/' ) :
                       self.links += [value]

        elif tag == 'title':
            #if we encounter a title tag, set the titleEncountered flag to True
            self.titleEncountered = True

    def handle_data(self, data):
        """
        For each data item found inbetween an opening and closing tag, this function will check if
        the self.titleEncountered flag is set to true, if titleEncountered is true this means we have found the title tag
        so we set self.title to the data, which will be the page title

        Parameters:
         data: the data to set self.title if self.titleEncountered is true
        """
        #if the titleEncountered flag is true, read the data after the tag
        if self.titleEncountered == True:
            self.title = data
            self.titleEncountered = False
            #set titleEncountered to false, so we dont read any more data

    def getPageInfo(self, url):
        """
        This function will instantiate the list and other variables needed to hold the link paths, page title and root URL
        It will then parse the page referred to by the url parameter, populating the links list and title variable

        Parameters:
         url: the url to be parsed
        """
        self.url = url
        self.links  = []
        self.title = ''
        parsed_uri = urlparse( url )
        self.domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

        self.titleEncountered = False
        with urllib.request.urlopen(url) as response:
            html = response.read()
            htmlString = html.decode("utf-8")
            self.feed(htmlString)
            #by converting the links list to a set and back to a list we can remove duplicate links
            self.links = list(set(self.links))

    def __str__(self):
        """
        This function will return a string representation of the page with the URL, title and list of links
        so the object can be printed 
        """
        return 'Title: {}\n URL: {}\n Links:  \n{}'.format(self.title,self.url,'\n'.join(self.links))

def getPageInfo(url):
    """
    This function takes a base URL and creates a parser for the base URL, it will then parse
    all pages linked to by the base URL, giving a crawl depth of 1
    This function could be run recursively with every URL found by the crawler, getting all pages in a domain
    but this may cause the server to refuse your connection as it makes too many requests if the site has a complex structure

    Parameter:
     url: the URL of the site to be parsed
    """

    if url[:4] != 'http':
        print('The URL string should have a protocol - either HTTP or HTTPS')
        raise SystemExit()

    #cut the '/' off the end of the URL 
    url = url[:-1] if url[-1] == '/' else url
    parser = LinkParser()
    parser.getPageInfo(url)
    parsers = []
    parsers.append(parser)

    #this will parse all pages in the url's list of links to a depth of 1
   
    for path in parser.links:
        #if the path is not a full URL, append it to the base URL
        newpath = path if path[:4] == 'http' else url + path
        newparser = LinkParser()
        newparser.getPageInfo(newpath)
        parsers.append(newparser)
        
    return parsers

if __name__ == "__main__":
    if(len(sys.argv) < 2):
        print('This program requires a URL string as its argument')
        raise SystemExit()
    
    pages = getPageInfo(sys.argv[1])
    for page in pages:
        print('\n',page)





